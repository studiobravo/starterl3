<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Common Language Lines
    |--------------------------------------------------------------------------
    |
    */
    'yes' => 'sim',
    'no' => 'não',
    'new' => 'novo',
    'edit' => 'editar',
    'update' => 'atualizar',
    'save' => 'salvar',
    'saved' => 'salvo',
    'updated' => 'atualizado',
    'successfully' => 'com sucesso',
    'cancel' => 'cancelar',
    'delete' => 'remover',
    'deleted' => 'removido',
    'remove' => 'remover',
    'removed' => 'removido',
    'done' => 'finalizado',
    'copy' => 'copiar',
    'export' => 'exportar',
    'print' => 'imprimir',
    'reset' => 'redefinir',
    'reload' => 'recarregar',
    'back' => 'voltar',
    'forward' => 'avançar',
    'search' => 'buscar',
    'view' => 'visualizar',
    'show' => 'exibir',
    'hide' => 'ocultar',
    'not-found' => 'não encontrado',
    'are-you-sure' => 'você tem certeza',
    'you-will-not-be-able-to-recover-this-registry' => 'você não poderá mais recuperar este registro',
    'sign-in-to-start-your-session' => 'efetue o acesso para iniciar sua sessão',
    'password' => 'senha',
    'email' => 'e-mail',
    'sign-in' => 'acessar',
    'confirm' => 'confirmar',
    'send'=>'enviar',


];
